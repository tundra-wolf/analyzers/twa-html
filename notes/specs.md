# HTML Analyzer - Specs

## HTML 5 

* [HTML Living Standard](https://html.spec.whatwg.org/print.pdf) on the WHATWG website.

## HTML 4

* [HTML 4.0 Specification](https://www.w3.org/TR/1998/REC-html40-19980424/html40.pdf) on the World Wide Web Consortium website.

