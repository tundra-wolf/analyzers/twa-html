use clap::ValueEnum;

/// Supported HTML versions.
#[derive(Clone, Debug, ValueEnum)]
pub enum HtmlVersion {
    /// HTML version 4.01
    Html4,

    /// HTML version 5.2
    Html5,

    /// XHTML version 4.01
    XHtml4,

    /// XHTML version 5.2
    XHtml5,
}

impl Default for HtmlVersion {
    fn default() -> Self {
        Self::Html4
    }
}
