//! HTML Parser Tests
//!
//! This program tests the parsing code of the this plugin. It covers correct and
//! incorrect input for both HTML4 and HTML5.
//!
//! The documents to be parsed are in the `documents/` folder which is divided as
//! follows.
//!
//! * all-versions - Tests for all supported versions
//! * html 4 - HTML 4 specific documents
//! * html 5 - HTML 5 specific documents
//!
//! Each of these is divided in the following folders
//!
//!     * <root> - Correct documents
//!     * errors - Incorrect documents that contain errors
//!     * warnings - Incorrect documents that contain warnings
//!
//! With each of these furhter divided in:
//!
//!         * full - Full documents
//!         * parts - Parts of documents to test specific tags/pieces

// Full document tests
//

#[test]
fn t_parse_minimal_html4_document() {
    assert_eq!(1, 1)
}

// Partial document tests
//

#[test]
#[should_panic]
fn t_parse_unterminated_comment() {
    assert_eq!(1, 2)
}
